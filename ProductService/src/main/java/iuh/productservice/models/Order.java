package iuh.productservice.models;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "orders")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private long orderId;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "total_amount")
    private double totalAmount;

    // Constructor
    public Order(long order_id, String customer_id, double total_amount) {
        this.orderId = order_id;
        this.customerId = customer_id;
        this.totalAmount = total_amount;
    }

    public Order() {

    }

    // Getters và Setters
    public long getOrder_id() {
        return orderId;
    }

    public void setOrder_id(long order_id) {
        this.orderId = order_id;
    }

    public String getCustomer_id() {
        return customerId;
    }

    public void setCustomer_id(String customer_id) {
        this.customerId = customer_id;
    }

    public double getTotal_amount() {
        return totalAmount;
    }

    public void setTotal_amount(double total_amount) {
        this.totalAmount = total_amount;
    }
}
package iuh.productservice.controllers;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import iuh.productservice.models.Order;
import iuh.productservice.models.Product;
import iuh.productservice.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController()
@RequestMapping("product")
public class ProductController {
    @Autowired
    ProductService productService;
    RestTemplate restTemplate = new RestTemplate();
    @Autowired
    RetryTemplate retryTemplate;

    @RateLimiter(name = "default")
    @GetMapping("/products")
    public List<Product> getProducts() {
        List<Product> products = productService.getProducts();
        return products;
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody Product product) {
            return productService.saveProduct(product);
    }

    @GetMapping("/getProductById/{id}")
    public Product getProduct(@PathVariable("id") Long id) {
        return productService.getProductById(id);
    }
    @Retryable(include = ResourceAccessException.class)
    @GetMapping("/getOrder/{id}")
    public Order getOrder(@PathVariable("id") Long id) {
        retryTemplate.execute(ctx -> {
            System.out.println("Retry");
            return restTemplate.getForObject("http://localhost:8083/order/getOrderById/" + id, Order.class);
        });
        throw new ResourceAccessException("Không thể kết nối đến OrderService");
        }
    }

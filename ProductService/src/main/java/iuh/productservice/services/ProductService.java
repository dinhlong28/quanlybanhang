package iuh.productservice.services;

import iuh.productservice.models.Product;
import iuh.productservice.repositories.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    public Product saveProduct(Product product){
        return productRepository.save(product);
    }
    public List<Product> getProducts(){
        return  productRepository.findAll();
    }
    public Product getProductById(long id){
        return productRepository.findById(id).get();
    }
}

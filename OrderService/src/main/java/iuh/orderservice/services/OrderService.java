package iuh.orderservice.services;

import iuh.orderservice.models.Order;
import iuh.orderservice.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    public final OrderRepository orderRepository;
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    public Order saveOrder(Order order){
        return orderRepository.save(order);
    }
    public Order getOrderById(long id){
        return orderRepository.findById(id).get();
    }
    public List<Order> getOrders(){
        return orderRepository.findAll();
    }
    public void deleteOrderById(long id){
        orderRepository.deleteById(id);
    }
}

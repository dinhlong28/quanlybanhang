package iuh.orderservice.controllers;

import iuh.orderservice.models.Order;
import iuh.orderservice.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/orders")
    public List<Order> getOrders() {
        List<Order> orders = orderService.getOrders();
        return orders;
    }
    @PostMapping("/addOrder")
    public Order saveOrder(@RequestBody Order order) {
        return orderService.saveOrder(order);
    }
    @GetMapping("/getOrderById/{id}")
    public Order getOrderById(@PathVariable("id") Long id) {
        return orderService.getOrderById(id);
    }
}

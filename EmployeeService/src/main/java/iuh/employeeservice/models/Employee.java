package iuh.employeeservice.models;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "employees")
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private long employee_id;

    @Column(name = "employee_name")
    private String employee_name;

    @Column(name = "salary")
    private double salary;

    @Column(name = "password")
    private String password;
    public Employee(long employee_id, String employee_name, double salary, String password) {
        this.employee_id = employee_id;
        this.employee_name = employee_name;
        this.salary = salary;
        this.password = password;
    }

    public Employee() {

    }

    // Getters và Setters
    public long getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(long employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}